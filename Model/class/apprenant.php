<?php

/**
 * Classe Apprenant enregistré
 */
class Apprenant
{
  protected $id;
  protected $nom;
  protected $prenom;
  protected $login;
  protected $photo;  
  protected $listAbsence;
  

  function __construct($id = null, $nom = null, $prenom = null, $login = null, $photo= null)
  {
    if($id == null)
    {
      $this->nom = $nom;
      $this->prenom = $prenom;
      $this->login = $login;
      $this->photo = $photo;      
      $this->listAbsence=array();   
    }
    else
    {
      $this->id = $id;
      $this->load();
    }
  }
  public function getId() { return $this->id; }
  public function getNom() { return $this->nom; }
  public function getPrenom() { return $this->prenom; }
  public function getLogin() { return $this->login; }
  public function getPhoto() { return $this->photo; }  
  public function getListAbsence() { return $this->listAbsence; }

  public function setId($id) { $this->id = $id; }
  public function setNom($nom) { $this->nom = $nom; }
  public function setPrenom($prenom) { $this->prenom = $prenom; }
  public function setLogin($login) { $this->login = $login; }
  public function setPhoto($photo) { $this->photo = $photo; }
  


  public function addAbsence($absence)
  {
    $this->listAbsence[]=$absence;
  }

  private function load()
  {
    require('bdd.php');
    $requete = $db->prepare("SELECT * FROM Apprenant WHERE id = ?");
    $requete->bindParam(1, $this->id);

    if($requete->execute()==false)
    {
      die('Il y a eu un problème lors de la récupération des informations utilisateur');
    }
    else
    {
      $infos = $requete->fetch(PDO::FETCH_ASSOC);
      $this->nom = $infos['nom'];
      $this->prenom   = $infos['prenom'];
      $this->login   = $infos['login'];
      $this->photo   = $infos['photo'];
      $this->idPromo  = $infos['idPromo'];
    }
  }

  public function save($idPromo)
  {
    require('bdd.php');
    $requete = $db->prepare("INSERT INTO Apprenant (nom, prenom, login, photo, idPromo) VALUES (?,?,?,?,?)");
    $requete->bindParam(1, $this->nom);
    $requete->bindParam(2, $this->prenom);
    $requete->bindParam(3, $this->login);
    $requete->bindParam(4, $this->photo);
    $requete->bindParam(5, $idPromo);

    if($requete->execute()==false)
    {
      die('Il y a eu un problème lors de l\'enregistrement des informations utilisateur');
    }
    else
    {
      $this->id = $db->lastInsertId();
    }
  }

  public function update()
  {
    require('bdd.php');
    $requete = $db->prepare("UPDATE Apprenant SET nom = ?, prenom = ?, login= ?, photo= ?, idPromo =? WHERE id = ?");
    $requete->bindParam(1, $this->nom);
    $requete->bindParam(2, $this->prenom);
    $requete->bindParam(3, $this->login);
    $requete->bindParam(4, $this->photo);    
    $requete->bindParam(5, $this->id);

    if($requete->execute()==false)
    {
      die('Il y a eu un problème lors de l\'enregistrement des informations utilisateur');
    }
  }

  static function apprenantExist($login)
  {
    require('bdd.php');
    $apprenant=null;
    $requete = $db->prepare("SELECT id FROM Apprenant WHERE login = ?");
    $requete->bindParam(1,$login);

    if($requete->execute()==false)
    {
      die('il y a souci de requete');
    }
    else
    {
      if ($requete->rowCount()==1){
        $ligne=$requete->fetch();
        $apprenant= new Apprenant($ligne['id']);
      }
    }
    return $apprenant;
  }

  static function getListApprenant($idPromo)
  {
    require('bdd.php');
    $liste = array();
    $requete = 'SELECT id FROM Apprenant WHERE idPromo=?';
    $req = $db->prepare($requete);
    $req->bindParam(1,$idPromo);

    if($req->execute()==false)
    {
      die('erreur : impossible de récupérer la liste');
    }
    else
    {      
      while($inf = $req->fetch(PDO::FETCH_ASSOC)){
        $liste[] = new Apprenant($inf['id']);
      }
    }
    return $liste;
  }
}

?>