<?php
/**
 * Classe formateur enregistré
 */

class Formateur{

  protected $id;
	protected $nom;
  protected $prenom;
	protected $login;
    
  function __construct($id=null, $nom=null, $prenom=null, $login=null)
  {
    if($id == null)
    {
      $this->nom          = $nom;
      $this->prenom       = $prenom;
      $this->login        = $login;
    }
    else
    {
      $this->id = $id;
      $this->load();
    } 
  }

  public function getId(){return $this->id;}
  public function getNom(){return $this->nom;}
  public function getPrenom(){return $this->prenom;}
  public function getLogin(){return $this->login;}
        
  public function setNom($nom) {$this->nom=$nom;}
  public function setPrenom($prenom) {$this->prenom=$prenom;}
  public function setLogin($login){ $this->login=$login;}

  private function load()
  {
    require('bdd.php');
    $requete = $db->prepare("SELECT * FROM Formateur WHERE id = ?");
    $requete->bindParam(1, $this->id);

    if($requete->execute()==false)
    {
      die('Il y a eu un problème lors de la récupération des informations');
    }
    else
    {
      $infos = $requete->fetch(PDO::FETCH_ASSOC);
      $this->nom     = $infos['nom'];
      $this->prenom  = $infos['prenom'];
      $this->login   = $infos['login'];    
    }
  }
  
  public function save()
	{
		require('bdd.php');
		$requete = $db->prepare('INSERT INTO Formateur (nom,prenom,login) values(?,?,?)');
		$requete->bindParam(1,$this->nom);
		$requete->bindParam(2,$this->prenom);
		$requete->bindParam(3,$this->login);	

		if($requete->execute()==false){
			echo 't\'es nulle 1';
			die;
		}
		else{
			$this->id=$db->lastInsertId();
		}
	}

  public function update()
	{
		require('bdd.php');
		$requete = $db->prepare("UPDATE Formateur	SET nom = ?, prenom = ?, login = ? WHERE id = ?");
		$requete->bindParam(1, $this->nom);
		$requete->bindParam(2, $this->prenom);
		$requete->bindParam(3, $this->login);
    $requete->bindParam(4,$this->id);

		if($requete->execute()==false)
		{
			die('Il y a eu un problème lors de l\'enregistrement des informations utilisateur');
		}
	}
  static function formateurExist($login)
  {
    require('bdd.php');
    $formateur= null;
    $requete = $db->prepare("SELECT id FROM Formateur WHERE login = ? ");
    $requete->bindParam(1,$login);

    if($requete->execute()==false)
    {
      die('il y a souci de requete');
    }
    else
    {
      if ($requete->rowCount()==1){
        $ligne=$requete->fetch();
        $formateur= new Formateur($ligne['id']);
      }
    }
    return $formateur;
  }
 
  static function getListForm()
  {
    require('bdd.php');
    $liste = array();
    $requete = 'SELECT id FROM Formateur';
    $req = $db->prepare($requete);
    $req->bindParam(1,$id);

    if($req->execute()==false)
    {
      die('erreur : impossible de récupérer la liste');
    }
    else
    {      
      while($inf = $req->fetch(PDO::FETCH_ASSOC)){
        $liste[] = new Formateur($inf['id']);
      }
    }
    return $liste;
  }

}
?>