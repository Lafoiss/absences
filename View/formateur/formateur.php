<?php

/**
 *
 */

Class ViewFormateur{

  function __construct()
  {
  }
  
  public function displayChoixPromo($promos=null){
    echo'
        <div class="formFormateur">
            <img id="logo" src="../Images/logo.png" alt="Logo de Simplon">
            <div id="titreFormateur">
                <h1>CHOIX DE LA FABRIQUE</h1>
            </div>
            <form class="form_formateur" action="" method="post">
                <div class="divLogin">
                   <select name="idPromo">';
                    foreach($promos as $promo){
                    echo '<option class="promo" value="'.$promo->getId().'">'.$promo->getLibelle()." ".$promo->getLieu().'</option>';
                    }
                    echo'
                  </select>
                </div>
                
                <button class="bouton_connexion" type="submit" name="choix" value="choosePromo">Valider</button>
            </form>
        </div>
        <script type="text/javascript" src="../View/formateur/formateur.js"></script>
    ';
  }

  public function displayFormApprenant() {
    echo '
      <header>
        <div class="logo_header">           
          <img class="logoHeader" src="../Images/logo.png" alt="Logo de Simplon">          
        </div>
        <div id="titreAjoutApprenant">
          <h1>Ajouter un apprenant</h1>
        </div>
      </header>
      <div id="formApprenant">
        <form id="form_Apprenant" action="" method="post">
          <div id="nom">            
            <label for="nom">Nom</label>
            <input type="text" name="nom" value="" required>
          </div>
          <div id="prenom">
            <label for="prenom">Prenom</label>
            <input type="text"  name="prenom" value="" required>
          </div>            
          <div id="login">            
            <label for="login">Login</label>
            <input type="text" name="login" value="" required>            
          </div>          
          <div id="photo">
            <label for="photo">Photo</label>
            <input type="text" name="photo" value="" />
          </div>          
          <button id="valider" type="submit" name="choix" value="addApprenant" onclick="return confirm(\'Êtes-vous sûr de vos informations\');">Valider</button>
        </form>
      </div>';
    }

}
?>