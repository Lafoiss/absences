<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Absence Simplon</title>
    <meta charset="utf-8">
    <!-- <link rel="icon" type="image/png" href="../Images/logo.png" /> -->
    <link rel="stylesheet" type="text/css" href="../View/admin/admin.css">
    <link href="https://fonts.googleapis.com/css?family=Asap|Overpass&display=swap" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Indie+Flower%7CLobster+Two&display=swap" rel="stylesheet">  -->
  </head>
  <body>
<?php
  require_once ('View/formateur/formateur.php');
  require_once ('Model/class/formateur.php');
  require_once ('View/apprenant/apprenant.php');
  require_once ('Model/class/apprenant.php');
  require_once ('View/appli/appli.php');
  require_once ('Model/class/promo.php');
  require_once ('Model/class/formateurPromo.php');
  $viewAppli = new ViewAppli();
  $viewApprenant= new ViewApprenant();
  $viewFormateur= new ViewFormateur();

  $choix='connexion';

  if (isset($_REQUEST['choix'])){$choix = $_REQUEST['choix'];}

  switch ($choix)
  {
    case 'connexion':$viewAppli->displayFormLogin(); //ok
    break;

    case 'connect': //ok
      if(isset($_POST['login'])){
        
        $formateur = Formateur::formateurExist($_POST['login']);
        $apprenant = Apprenant::apprenantExist($_POST['login']);
        if($apprenant != null)
        {
          $_SESSION['idApprenant'] = $apprenant->getId();
          $viewApprenant->displayFormAbsence();
        }
        else if($formateur != null)
        {
          $_SESSION['idFormateur'] = $formateur->getId();
                    
          echo $formateur->getNom();
          $promo = formateurPromo::getListPromoForm($formateur->getId());
          $viewFormateur->displayChoixPromo($promo);
        }
        else{
          echo 'Mauvais login';
          $viewAppli->displayFormLogin();
        } 
      }       
      break;
      case 'choosePromo':  
        $_SESSION['idPromo'] = $_REQUEST['idPromo'];
        $promo= new Promo($_REQUEST['idPromo']);
        print_r($promo->getListApprenant());
        echo $viewFormateur->displayFormApprenant();
        $formateur = $promo->getFormateur($_SESSION['idFormateur']);
        echo $formateur->getNom()." ".$promo->getLibelle();
      break;
      case  'addApprenant':
        $apprenant= new Apprenant(
        null,
        $_POST['nom'],
        $_POST['prenom'],
        $_POST['login'],
        $_POST['photo']        
        );
        $apprenant->save($_SESSION['idPromo']);
    
      break;
       
}
  ?>
    </body>
</html>

