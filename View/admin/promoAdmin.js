class PromoAdmin{

  constructor(){
    $("#ville").autocomplete({
      source: function (request, response) {
        $.ajax({
          url: "https://vicopo.selfbuild.fr/ville/"+request.term,
          dataType: "json",
          success: function (data) {
            response($.map(data.cities, function (item) {
              return { label: item.city,
                       value: item.city
              };
            }));
          }
        });
      },
    });
    $("#dateDeb").datepicker({
      dateFormat: "dd-mm-yy",     
      onSelect: function (date) {
          let date2 = $('#dateDeb').datepicker('getDate');
          // date2.setDate(date2.getDate() + 1);
          // $('#dateFin').datepicker('setDate', date2);
          //sets minDate to dateDeb date + 1   
          $('#dateFin').datepicker({
            dateFormat: "dd-mm-yy",
            minDate:$('#dateDeb').datepicker('getDate'),
            onClose: function () {
              let dateDeb = $('#dateDeb').datepicker('getDate');
              console.log(dateDeb);
              let dateFin = $('#dateFin').datepicker('getDate');
              if (dateFin <= dateDeb) {
                $('#dateFin').datepicker('setDate', dateDeb);
              }
            }
          });       
      }
    });
    
    
    this.equipe = $('#equipe');
    
    this.compteur = 0;

    $('#add').click(function() {
      $.ajax({

          url: "adminControl.php?choix=addFormateur",
          dataType: "html",
          type:'post',
          success: function (data) {
            $('#equipe').append(data);
            $('.remove').click(function(){
              $(this).parent().remove();
            });
          }
      });
      
      
        // this.remove = $('.remove');       
           
        // this.remove.click( function() {
        //   this.closest(".equipe_input").remove();
         
        // });
      
    });

    $('#choose').click(function(){     
      
      $.ajax({

          url: "adminControl.php?choix=chooseFormateur&idFormateur="+$('#listeFormateur').val(),
          dataType: "html",
          type:'post',
          success: function (data) {
            $('#equipe').append(data);
            $('.remove').click(function(){
              $(this).parent().remove();
            });
          }
      });
    });
    
  }
}
let promoAdmin = new PromoAdmin();
