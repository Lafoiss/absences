<?php

/**
 *
 */
class ViewAppli
{

  function __construct() {

  }

  public function displayFormLogin() {
    echo '
      <div class="formAdmin">
        <img id="logo" src="../Images/logo.png" alt="Logo de Simplon">
        <div class="titreAdmin">
          <h1>IDENTIFIEZ VOUS</h1>
        </div>
        <form class="form_admin" action="" method="post">
          <div class="divLogin">
            <label for="login">Identifiant</label>
            <input class="loginAdmin" type="text" name="login" value="" required>
          </div>          
          <button class="bouton_connexion" type="submit" name="choix" value="connect">Valider</button>
        </form>
      </div>
      <script type="text/javascript" src="../View/admin/loginAdmin.js"></script>
    ';
  }
}
