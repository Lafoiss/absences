<?php

/**
 * Classe Absence
 */
class Absence
{

  protected $id;
  protected $motif;
  protected $dateHeureDeb;
  protected $dateHeureFin;
  protected $commentaire;
  protected $justificatif;
  protected $commentResponsable;
  protected $idApprenant;

  function __construct($id = null, $motif = null, $dateHeureDeb = null, $dateHeureFin = null, $commentaire= null, $justificatif = null, $commentResponsable = null, $idApprenant = null)
  {
    if($id == null)
    {
      $this->motif = $motif;
      $this->dateHeureDeb = $dateHeureDeb;
      $this->dateHeureFin = $dateHeureFin;
      $this->commentaire = $commentaire;
      $this->justificatif = $justificatif;
      $this->commentResponsable =$commentResponsable;
      $this->idApprenant = $idApprenant;

    }
    else
    {
      $this->id = $id;
      $this->load();
    }
  }

  private function load()
  {
    require('bdd.php');
    $requete = $db->prepare("SELECT * FROM Absence WHERE id = ?");
    $requete->bindParam(1, $this->id);

    if($requete->execute()==false)
    {
      die('Il y a eu un problème lors de la récupération des informations');
    }
    else
    {
      $infos = $requete->fetch(PDO::FETCH_ASSOC);

      $this->motif = $infos['motif'];
      $this->dateHeureDeb   = $infos['dateHeureDeb'];
      $this->dateHeureFin  = $infos['dateHeureFin'];
      $this->commentaire   = $infos['commentaire'];
      $this->justificatif   = $infos['justificatif'];
      $this->commentResponsable   = $infos['commentResponsable'];
      $this->idApprenant = $idApprenant['idApprenant'];
    }
  }

  public function save()
  {
    require('bdd.php');
    $requete = $db->prepare(
      "INSERT INTO Absence (motif, dateHeureDeb, dateHeureFIn, commentaire, justificatif, commentResponsable, idApprenant)
      VALUES (?,?,?,?,?,?,?)"
    );
    $requete->bindParam(1, $this->motif);
    $requete->bindParam(2, $this->dateHeureDeb);
    $requete->bindParam(3, $this->dateHeureFin);
    $requete->bindParam(4, $this->commentaire);
    $requete->bindParam(5, $this->justificatif);
    $requete->bindParam(6, $this->commentResponsable);
    $requete->bindParam(7, $this->idApprenant);
    

    if($requete->execute()==false)
    {
      die('Il y a eu un problème lors de l\'enregistrement des informations');
    }
    else
    {
      $this->id = $db->lastInsertId();
    }
  }

  public function update()
  {
    require('bdd.php');
    $requete = $db->prepare(
      "UPDATE Absence
      SET motif = ?, dateHeureDeb = ?, dateHeureFin= ?, commentaire = ?, justificatif =?, commentResponsable =?, idApprenant =?
      WHERE id = ?"
    );
    $requete->bindParam(1, $this->motif);
    $requete->bindParam(2, $this->dateHeureDeb);
    $requete->bindParam(3, $this->dateHeureFin);
    $requete->bindParam(4, $this->commentaire);
    $requete->bindParam(5, $this->justificatif);
    $requete->bindParam(6, $this->commentResponsable);
    $requete->bindParam(7, $this->idAprenant);
    $requete->bindParam(7, $this->id);

    if($requete->execute()==false)
    {
      die('Il y a eu un problème lors de l\'enregistrement des informations');
    }
  }
 

  public function getId() { return $this->id; }
  public function getMotif() { return $this->motif; }
  public function getDateHeureDeb() { return $this->dateHeureDeb; }
  public function getDateHeureFin() { return $this->dateHeureFin; }
  public function getCommentaire() { return $this->commentaire; }
  public function getJustificatif() { return $this->justificatif; }
  public function getCommentResponsable() { return $this->commentResponsable; }


  public function setId($id) { $this->id = $id; }
  public function setMotif($motif) { $this->motif = $motif; }
  public function setDateHeureDeb($dateHeureDeb) { $this->dateHeureFin = $dateHeureDeb; }
  public function setDateHeureFin($dateHeureFin) { $this->$dateHeureFin = $dateHeureFin; }
  public function setCommentaire($commentaire) { $this->commentaire = $commentaire; }
  public function setJustificatif($justificatif) { $this->justificatif = $justificatif; }
  public function setCommentResponsable($commentResponsable) { $this->commentResponsable = $commentResponsable; }

}

?>