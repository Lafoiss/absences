<?php

/**
 * Classe admin enregistré
 */
class Admin
{
	protected $id;
	protected $login;
	protected $mdp;

	function __construct($id = null, $login = null, $mdp = null)
	{
		if($id == null)
		{
			$this->login = $login;
			$this->mdp	 = $mdp;
		}
		else
		{
			$this->id = $id;
			$this->load();
		}
	}

	private function load()
	{
		require('bdd.php');
		$requete = $db->prepare("SELECT * FROM Admin WHERE id = ?");
		$requete->bindParam(1, $this->id);

		if($requete->execute()==false)
		{
			die('Il y a eu un problème lors de la récupération des informations utilisateur');
		}
		else
		{
			$infos = $requete->fetch(PDO::FETCH_ASSOC);
			$this->login = $infos['login'];
			$this->mdp	 = $infos['mdp'];
		}
	}

	public function save()
	{
		require('bdd.php');
		$requete = $db->prepare("INSERT INTO Admin (login, mdp) VALUES (?,?)");
		$requete->bindParam(1, $this->login);
		$requete->bindParam(2, $this->mdp);

		if($requete->execute()==false)
		{
			die('Il y a eu un problème lors de l\'enregistrement des informations utilisateur');
		}
		else
		{
			$this->id = $db->lastInsertId();
		}
	}

	public function update()
	{
		require('bdd.php');
		$requete = $db->prepare("UPDATE Admin SET login = ?, mdp = ? WHERE id = ?");
		$requete->bindParam(1, $this->login);
		$requete->bindParam(2, $this->mdp);
		$requete->bindParam(3, $this->id);

		if($requete->execute()==false)
		{
			die('Il y a eu un problème lors de l\'enregistrement des informations utilisateur');
		}
	}

	static function adminExist($login, $mdp)
	{
		require('bdd.php');
		$requete = $db->prepare("SELECT * FROM Admin WHERE login = ?  AND mdp = ?");
		$requete->bindParam(1, $login);
		$requete->bindParam(2, $mdp);
		if($requete->execute()==false)
		{
			die('il y a souci de requete');
		}
		return $requete->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getId() { return $this->id; }
	public function getLogin() { return $this->login; }
	public function getMdp() { return $this->mdp; }

	public function setId($id) { $this->id = $id; }
	public function setLogin($login) { $this->login = $login; }
	public function setMdp($mdp) { $this->mdp = $mdp; }

}
?>
