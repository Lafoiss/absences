<?php
/**
 * Classe FormateurPromo
 */
require_once('formateur.php');

class FormateurPromo{

  protected $id;
  protected $idFormateur;
  protected $idPromo;

  function __construct($id=null, $idFormateur=null, $idPromo=null)
  {
    if($id == null)
    {
      $this->idFormateur      = $idFormateur;
      $this->idPromo          = $idPromo;
    }
    else
    {
      $this->id = $id;
      $this->load();
    } 
  }

  public function getId(){return $this->id;}
  public function getidFormateur(){return $this->idFormateur;}
  public function getidPromo(){return $this->idPromo;}
       
  public function setidFormateur($idFormateur) {$this->idFormateur=$idFormateur;}
  public function setidPromo($idPromo) {$this->idPromo=$idPromo;}
    
  private function load()
  {
    require('bdd.php');
    $requete = $db->prepare("SELECT * FROM FormateurPromo WHERE id = ?");
    $requete->bindParam(1, $this->id);
    if($requete->execute()==false)
    {
      die('Il y a eu un problème lors de la récupération des informations');
    }
    else
    {
      $infos = $requete->fetch(PDO::FETCH_ASSOC);
      $this->idFormateur     = $infos['idFormateur'];
      $this->idPromo  = $infos['idPromo'];
    }
  }

  static function save($idPromo,$listFormateur)
  {
    require('bdd.php');
    $requete =$db->prepare('DELETE FROM FormateurPromo WHERE idPromo =?' );
    $requete->bindParam(1,$idPromo);
    if($requete->execute()==false){
        echo 'ton delete c\'est de la m';
        die;
    }
    $requete = $db->prepare('INSERT INTO FormateurPromo (idFormateur, idPromo) values(?,?)');
    foreach ($listFormateur as $formateur){
      $idFormateur=$formateur->getId();
      $requete->bindParam(1,$idFormateur);
      $requete->bindParam(2,$idPromo);
      if($requete->execute()==false){
        echo 't\'es nulle 1';
        die;
      }
    } 
  }
  
  static function getListFormateur($idPromo)
  {
    require('bdd.php');
    $liste = array();
    $requete = 'SELECT idFormateur FROM FormateurPromo WHERE idPromo=?';
    $req = $db->prepare($requete);
    $req->bindParam(1,$idPromo);
    if($req->execute()==false)
    {
      die('erreur : impossible de récupérer la liste');
    }
    else
    {      
      while($inf = $req->fetch(PDO::FETCH_ASSOC))
      {
        $liste[] = new Formateur($inf['idFormateur']);
      }
    }
    return $liste;
  }

  static function getListPromoForm($idFormateur)
  {
    require('bdd.php');
    $listePromo = array();
    $requete = 'SELECT idPromo FROM FormateurPromo WHERE idFormateur=?';
    $req = $db->prepare($requete);
    $req->bindParam(1,$idFormateur);
    if($req->execute()==false)
    {
      die('erreur : impossible de récupérer la liste Promo');
    }
    else
    {      
      while($inf = $req->fetch(PDO::FETCH_ASSOC))
      {
        $listePromo[] = new Promo($inf['idPromo']);
      }
    }
    return $listePromo;
  }

}
?>